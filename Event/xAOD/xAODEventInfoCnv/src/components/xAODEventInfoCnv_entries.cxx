#include "../EventInfoCnvTool.h"
#include "../EventInfoSelectorTool.h"
#include "../EventInfoCnvAlg.h"
#include "../EventInfoReaderAlg.h"
#include "../EventDuplicateFinderAlg.h"

#include "../EventInfoMCWeightFixAlg.h"

DECLARE_COMPONENT( xAODMaker::EventInfoCnvTool )
DECLARE_COMPONENT( xAODMaker::EventInfoSelectorTool )
DECLARE_COMPONENT( xAODMaker::EventInfoCnvAlg )
DECLARE_COMPONENT( xAODReader::EventInfoReaderAlg )
DECLARE_COMPONENT( xAODReader::EventDuplicateFinderAlg )

DECLARE_COMPONENT( EventInfoMCWeightFixAlg )

